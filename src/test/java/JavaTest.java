/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

import com.technicallyvalid.kollapse.*;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

public class JavaTest {
    @Test
    public void sanity() throws IOException {
        SimpleTiledModelSampleParameters instanceAndRunnerParams = new SimpleTiledModelSampleParameters("Circles");
        instanceAndRunnerParams.setWidth(24);
        instanceAndRunnerParams.setHeight(24);

        SimpleTiledModelParameters modelParams = new SimpleTiledModelParameters();
        modelParams.setTileSize(32);
        modelParams.setTileToFile(tile -> Paths.get("WaveFunctionCollapse/samples/Circles/" + tile.getName() + ".png"));
        modelParams.setTiles(Arrays.asList(
                new Tile("b_half", "T")
        ));
        modelParams.setNeighbors(Arrays.asList(
                new Neighbor("b_half", "b_half")
        ));
        SimpleTiledModel model = new SimpleTiledModel(modelParams, instanceAndRunnerParams);

        Instant start = Instant.now();
        Runner runner = new Runner(instanceAndRunnerParams);
        ModelResult result = runner.run(model).get(0);
        if (result.getSuccess()) {
            Instant finishedTime = Instant.now();
            System.out.println(String.format("Took %dms", Duration.between(start, finishedTime).toMillis()));
            BufferedImage image = model.renderTo(AwtRenderer.INSTANCE);
            ImageIO.write(image, "png", new File("saved.png"));
            System.out.println("DONE");
        } else {
            System.err.println("CONTRADICTION");
        }
    }
}
