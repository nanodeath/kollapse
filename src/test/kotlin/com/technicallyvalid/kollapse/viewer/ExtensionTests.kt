package com.technicallyvalid.kollapse.viewer

import javafx.scene.image.WritableImage
import javafx.scene.paint.Color
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.Test
import java.time.Duration
import java.time.Instant

class ExtensionTests {
    @Test
    fun toTwoDimensions() {
        // [1,2,3,4] -> [[1,2],[3,4]]
        val output = intArrayOf(1, 2, 3, 4).toTwoDimensions(2)
        assertThat(output).isEqualTo(arrayOf(intArrayOf(1, 2), intArrayOf(3, 4)))

        // [1,2,3,4,5,6,7,8,9] -> [[1,2,3],[4,5,6],[7,8,9]]
        val output2 = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9).toTwoDimensions(3)
        assertThat(output2).isEqualTo(arrayOf(intArrayOf(1, 2, 3), intArrayOf(4, 5, 6), intArrayOf(7, 8, 9)))
    }

    @Test
    fun twoDimensionsIllegal() {
        // [1, 2, 3] =!> [[1, 2], [3]]
        assertThatThrownBy { intArrayOf(1, 2, 3).toTwoDimensions(2) }
            .isInstanceOf(IllegalArgumentException::class.java)
    }

    @Test
    fun toOneDimension() {
        // [1,2,3,4] -> [[1,2],[3,4]]
        val output = arrayOf(intArrayOf(1, 2), intArrayOf(3, 4)).toOneDimension()
        assertThat(output).isEqualTo(intArrayOf(1, 2, 3, 4))

        // [1,2,3,4,5,6,7,8,9] -> [[1,2,3],[4,5,6],[7,8,9]]
        val output2 = arrayOf(intArrayOf(1, 2, 3), intArrayOf(4, 5, 6), intArrayOf(7, 8, 9)).toOneDimension()
        assertThat(output2).isEqualTo(intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9))
    }

    @Test
    fun trivialZoom() {
        val image = WritableImage(1, 1).apply {
            pixelWriter.setColor(0, 0, Color.BLACK)
        }
        val zoomed = image.zoom(2)
        assertThat(zoomed.width).isEqualTo(2.0)
        assertThat(zoomed.height).isEqualTo(2.0)
        for (y in 0 until 2) for (x in 0 until 2) {
            assertThat(zoomed.pixelReader.getColor(x, y)).isEqualTo(Color.BLACK)
        }
    }


    @Test
    fun simpleZoom() {
        val image = WritableImage(2, 1).apply {
            pixelWriter.setColor(0, 0, Color.WHITE)
            pixelWriter.setColor(1, 0, Color.BLACK)
        }
        val zoomed = image.zoom(2)
        assertThat(zoomed.width).isEqualTo(4.0)
        assertThat(zoomed.height).isEqualTo(2.0)
        assertThat(zoomed.pixelReader.getColor(0, 0)).isEqualTo(Color.WHITE)
        assertThat(zoomed.pixelReader.getColor(1, 0)).isEqualTo(Color.WHITE)
        assertThat(zoomed.pixelReader.getColor(2, 0)).isEqualTo(Color.BLACK)
        assertThat(zoomed.pixelReader.getColor(3, 0)).isEqualTo(Color.BLACK)
    }

    @Test
    fun largeZoom() {
        val zoomFactor = 3
        val originalDimension = 500
        val image = WritableImage(originalDimension, originalDimension).apply {
            for (y in 0 until originalDimension) for (x in 0 until originalDimension) {
                if (x >= originalDimension / 2 && y >= originalDimension / 2) {
                    pixelWriter.setColor(x, y, Color.BLACK)
                } else {
                    pixelWriter.setColor(x, y, Color.WHITE)
                }
            }
        }
        val start = Instant.now()
        val zoomed = image.zoom(zoomFactor)
        val end = Instant.now()
        println("Resized ${originalDimension}px image to ${originalDimension * zoomFactor}px in ${Duration.between(start, end).toMillis()}ms")
        assertThat(zoomed.width).isEqualTo(originalDimension * zoomFactor.toDouble())
        assertThat(zoomed.height).isEqualTo(originalDimension * zoomFactor.toDouble())
        for (y in 0 until originalDimension * zoomFactor) for (x in 0 until originalDimension * zoomFactor) {
            if (x >= originalDimension * zoomFactor / 2 && y >= originalDimension * zoomFactor / 2) {
                assertThat(zoomed.pixelReader.getColor(x, y)).isEqualTo(Color.BLACK)
            } else {
                assertThat(zoomed.pixelReader.getColor(x, y)).isEqualTo(Color.WHITE)
            }
        }
    }
}
