/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

import BufferedImageAssert.Companion.assertThat
import com.technicallyvalid.kollapse.*
import org.junit.Test
import java.nio.file.Paths
import javax.imageio.ImageIO

class ConsistencyTest {
    // Overlapping + ground
    @Test
    fun overlappingCity() {
        val image = ImageIO.read(Paths.get("WaveFunctionCollapse", "samples", "City.png").toFile())
        val params = OverlappingModelParameters(image)
        val def = object : OverlappingModelInstanceParameters {
            override val name = "City"
            override val N = 3
            override val width = 48
            override val height = 48
            override val symmetry = 2
            override val periodicInput = true
            override val periodicOutput = true
            override val ground = -1
            override val observeCallback = {}
            override val propagateCallback = {}
        }
        val model = OverlappingModel(params, def)
        model.run(2)
        val output = model.renderTo(AwtRenderer)
        val expectedOutput = ImageIO.read(Paths.get("src", "test", "resources", "city_3.png").toFile())
        assertThat(output).hasSameContentAs(expectedOutput)
    }

    // Overlapping + no ground
    @Test
    fun overlappingSimpleMaze() {
        val image = ImageIO.read(Paths.get("WaveFunctionCollapse", "samples", "Simple Maze.png").toFile())
        val params = OverlappingModelParameters(image)
        val def = object : OverlappingModelInstanceParameters {
            override val name = "Simple Maze"
            override val N = 2
            override val width = 48
            override val height = 48
            override val symmetry = 8
            override val periodicInput = true
            override val periodicOutput = false
            override val ground = 0
            override val observeCallback = {}
            override val propagateCallback = {}
        }
        val model = OverlappingModel(params, def)
        model.run(0)
        val output = model.renderTo(AwtRenderer)
        val expectedOutput = ImageIO.read(Paths.get("src", "test", "resources", "simple_maze_2.png").toFile())
        assertThat(output).hasSameContentAs(expectedOutput)
    }

    // tiled
    @Test
    fun tiledCircles() {
        val params = kotlinSimpleModelCircles()
        val def = object : SimpleTiledModelInstanceParameters {
            override val name = "Circles"
            override val subset = null
            override val width = 24
            override val height = 24
            override val periodic = false
            override val black = false
        }
        val model = SimpleTiledModel(params, def)
        model.run(0)
        val output = model.renderTo(AwtRenderer)

        val expectedOutput = ImageIO.read(Paths.get("src", "test", "resources", "circles_24.png").toFile())
        assertThat(output).hasSameContentAs(expectedOutput)
    }
}