/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

import org.assertj.core.api.AbstractAssert
import java.awt.image.BufferedImage

class BufferedImageAssert(actual: BufferedImage) : AbstractAssert<BufferedImageAssert, BufferedImage>(actual, BufferedImageAssert::class.java) {
    companion object {
        fun assertThat(actual: BufferedImage) = BufferedImageAssert(actual)
    }

    fun hasSameContentAs(expected: BufferedImage): BufferedImageAssert {
        isNotNull()

        if (actual.width != expected.width || actual.height != expected.height) {
            failWithMessage("Expected image to have dimensions ${expected.width}×${expected.height}, but was ${actual.width}×${actual.height}!")
        }
        // now for the slow part, comparing individual pixels.
        var pixelDifferences = 0
        for (x in 0 until expected.width) for (y in 0 until expected.height) {
            if (actual.getRGB(x, y) != expected.getRGB(x, y)) {
                pixelDifferences++
            }
        }
        if (pixelDifferences > 0) {
            val totalPixels = actual.width * actual.height
            failWithMessage("Expected images to have identical pixels, but $pixelDifferences (of $totalPixels) were different!")
        }

        return this
    }
}