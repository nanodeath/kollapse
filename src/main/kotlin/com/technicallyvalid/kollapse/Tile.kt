/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

data class Tile @JvmOverloads constructor(val name: String, val symmetry: String? = null, val weight: Double? = null) {
    init {
        require(!name.contains(' ')) { "name should not contain spaces, was $name" }
    }
}