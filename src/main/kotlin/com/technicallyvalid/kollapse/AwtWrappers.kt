/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.awt.Color
import java.awt.image.BufferedImage

class AwtBufferedImageWrapper(override val image: BufferedImage) : ReadableImage<BufferedImage, AwtColorWrapper>, WritableImage<BufferedImage> {
    override val width = image.width
    override val height = image.height

    override fun getPixel(x: Int, y: Int) = AwtColorWrapper(Color(image.getRGB(x, y)))

    override fun setPixel(x: Int, y: Int, red: Int, green: Int, blue: Int) {
        image.setRGB(x, y, Color(red, green, blue).rgb)
    }
}

data class AwtColorWrapper(val color: Color) : Pixel {
    override val red = color.red
    override val green = color.green
    override val blue = color.blue
}

object AwtRenderer : Renderer<AwtBufferedImageWrapper> {
    override fun createImage(width: Int, height: Int) =
            AwtBufferedImageWrapper(BufferedImage(width, height, BufferedImage.TYPE_INT_RGB))
}