/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.prefs.Preferences

object Preferences {
    private const val recentFilesPreferenceName = "recentFiles"
    private const val delimiter = '|'
    const val defaultRecentFileLimit = 5

    /**
     * Returns a list of recent files. Could be empty. Most recent file is first.
     */
    fun getRecentFiles(): List<Path> {
        val value = prefs().get(recentFilesPreferenceName, null)
        return if (value != null) {
            value.split(delimiter).map { Paths.get(it) }.reversed()
        } else {
            emptyList()
        }
    }

    fun rememberRecentFile(path: Path, limit: Int = defaultRecentFileLimit) {
        val recentFiles = LinkedHashSet(getRecentFiles()).apply {
            remove(path)
            add(path)
        }.toList().takeLast(limit)
        saveRecentFiles(recentFiles)
    }

    fun forgetRecentFiles() {
        saveRecentFiles(emptyList())
    }

    private fun saveRecentFiles(current: List<Path>) {
        prefs().put(recentFilesPreferenceName, current.joinToString(delimiter.toString()))
    }

    private fun prefs() = Preferences.userNodeForPackage(MainViewerApp::class.java)
}