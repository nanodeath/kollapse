/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import com.technicallyvalid.kollapse.Pixel
import javafx.scene.paint.Color

data class JavaFXColor(val color: Color) : Pixel {
    // The colors are [0, 1.0] instead of [0, 255].
    override val red = (color.red * 255).round().toInt()
    override val green = (color.green * 255).round().toInt()
    override val blue = (color.blue * 255).round().toInt()

    private fun Double.round(): Long = Math.round(this)
}