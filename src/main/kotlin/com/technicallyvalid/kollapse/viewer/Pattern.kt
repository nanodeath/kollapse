/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import javafx.scene.paint.Color
import java.util.*

class Pattern(val data: Array<Array<Color>>) {
    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        }
        if (other !is Pattern) {
            return false
        }
        return Arrays.deepEquals(data, other.data)
    }

    override fun hashCode(): Int {
        return Arrays.deepHashCode(data)
    }

    override fun toString(): String {
        return Arrays.deepToString(data)
    }
}