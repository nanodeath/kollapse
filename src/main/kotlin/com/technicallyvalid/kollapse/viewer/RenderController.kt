/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import com.technicallyvalid.kollapse.AwtRenderer
import com.technicallyvalid.kollapse.OverlappingModel
import com.technicallyvalid.kollapse.OverlappingModelInstanceParameters
import com.technicallyvalid.kollapse.OverlappingModelParameters
import javafx.application.Platform
import javafx.concurrent.Task
import javafx.embed.swing.SwingFXUtils
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.image.WritableImage
import javafx.scene.text.Text
import javafx.stage.Modality
import javafx.stage.Stage
import java.time.Duration
import java.time.Instant
import java.util.*

class RenderController {
    @FXML private lateinit var image: ImageView
    @FXML private lateinit var progress: ProgressIndicator
    @FXML private lateinit var observations: Text
    @FXML private lateinit var propagations: Text
    @FXML private lateinit var timePerUpdate: Text
    @FXML private lateinit var showUpdates: CheckBox
    @FXML private lateinit var updatesPerFrame: ToggleGroup
    private var realUpdatesPerFrame = 0

    fun init(overlappingModelParams: OverlappingModelParameters, overlappingModelInstanceParams: OverlappingModelInstanceParameters, newStage: Stage) {
        realUpdatesPerFrame = (updatesPerFrame.selectedToggle as RadioButton).text.toInt()
        updatesPerFrame.selectedToggleProperty().addListener { observableValue, old, new ->
            realUpdatesPerFrame = (new as RadioButton).text.toInt()
        }
        val task = object : Task<Unit>() {
            private var generatedImage: WritableImage? = null

            private lateinit var model: OverlappingModel
            private var lastObservation = Instant.now()
            private var timeBetweenObservations = 0
            private val recentObservationDurations = ArrayDeque<Int>(101)

            private var observations = 0
            private var propagations = 0
            private var framesSinceLastRepaint = 1000

            private fun updateTimeBetweenObservations() {
                while (recentObservationDurations.size > realUpdatesPerFrame) recentObservationDurations.remove()
                timeBetweenObservations = recentObservationDurations.average().toInt()
            }

            fun updateUiNumbers() {
                updateTimeBetweenObservations()
                Platform.runLater {
                    this@RenderController.observations.text = observations.toString()
                    this@RenderController.propagations.text = propagations.toString()
                    timePerUpdate.text = "${timeBetweenObservations}ms"
                }
            }

            override fun call() {
                val realInstanceParams = object : OverlappingModelInstanceParameters by overlappingModelInstanceParams {
                    override val observeCallback = {
                        observations++
                        val time = Instant.now()
                        recentObservationDurations.add(Duration.between(lastObservation, time).toMillis().toInt())
                        lastObservation = time
                        updateUiNumbers()
                        if (showUpdates.isSelected) {
                            framesSinceLastRepaint++
                            if (framesSinceLastRepaint >= realUpdatesPerFrame) {
                                renderImage()
                                framesSinceLastRepaint = 0
                            }
                        }
                    }
                    override val propagateCallback: () -> Unit = {
                        propagations++
                    }
                }
                model = OverlappingModel(overlappingModelParams, realInstanceParams)

                this@RenderController.progress.isVisible = true
                val run = model.run(Random().nextLong())
                if (run) {
                    renderImage()
                } else {
                    generatedImage = null
                }
            }

            private fun renderImage() {
//                val start = Instant.now()
                val awtImage = model.renderTo(AwtRenderer)
//                val middle = Instant.now()
                generatedImage = WritableImage(awtImage.width, awtImage.height)
                SwingFXUtils.toFXImage(awtImage, generatedImage!!)
                generatedImage = generatedImage!!.zoom(4) as WritableImage
//                val end = Instant.now()
//                println("render took ${Duration.between(start, end).toMillis()}ms, of which ${Duration.between(middle, end).toMillis()}ms was basically wasted")
                Platform.runLater {
                    image.image = generatedImage
                    image.fitWidth = generatedImage!!.width
                    image.fitHeight = generatedImage!!.height
                }
            }

            override fun succeeded() {
                super.succeeded()
                this@RenderController.progress.isVisible = false
                if (generatedImage == null) {
                    Alert(Alert.AlertType.ERROR).apply {
                        contentText = "CONTRADICTION"
                        initModality(Modality.WINDOW_MODAL)
                        showAndWait()
                        newStage.close()
                    }
                }
            }
        }
        val thread = Thread(task)
        thread.isDaemon = true
        thread.start()
        newStage.onCloseRequest = EventHandler {
            task.cancel()
            thread.stop() // Yeah yeah...
        }
    }
}