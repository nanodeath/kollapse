/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import com.technicallyvalid.kollapse.ColorIndexer
import com.technicallyvalid.kollapse.OverlappingModelInstanceParameters
import com.technicallyvalid.kollapse.OverlappingModelParameters
import com.technicallyvalid.kollapse.PatternGenerator
import javafx.application.Platform
import javafx.embed.swing.SwingFXUtils
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.image.WritableImage
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import javafx.stage.FileChooser
import javafx.stage.Stage
import javafx.util.StringConverter
import java.awt.image.BufferedImage
import java.io.File
import java.util.*

class Controller {
    @FXML lateinit var image: ImageView
    @FXML lateinit var scrollPane: ScrollPane
    @FXML lateinit var sizeSlider: Slider
    @FXML lateinit var symmetrySlider: Slider
    @FXML lateinit var periodicCheckBox: CheckBox
    @FXML lateinit var indexFinderLabel: Label
    @FXML lateinit var indexFinderPreview: ImageView
    @FXML lateinit var fileMenu: Menu
    @FXML lateinit var statusBarLabel: Label
    @FXML lateinit var zoomSpinner: Spinner<Int>
    @FXML lateinit var groundCheckBox: CheckBox
    @FXML lateinit var groundValue: TextField

    lateinit var stage: Stage
    // Image that was loaded from disk
    private var originalImage: Image? = null
    // Zoomed image, without modification
    private var originalZoomedImage: Image? = null
    // Zoomed image, possibly with a region square drawn on top
    private var zoomedImage: Image? = null

    private val patterns = LinkedHashSet<Pattern>()
    // Size of the region we're tracking
    private var N: Int = 1
    private var lastPoint: Pair<Int, Int>? = null

    private var currentZoom: Int = 1

    fun updateIndexFinder(x: Int, y: Int, current: Pattern) {
        val idx = patterns.indexOf(current).let { idx ->
            if (idx < 0) "ⁿ/ₐ" else idx.toString()
        }
        indexFinderLabel.text = "Index: $idx"
        statusBarLabel.text = "x: $x y: $y Index: $idx"
    }

    fun calculatePatterns() {
        patterns.clear()

        originalImage?.let { originalImage ->
            // TODO colorIndex really only needs to be calculated once per image
            val colorIndex = ColorIndexer(JavaFXReadableImage(originalImage))
            val periodicInput = periodicCheckBox.isSelected
            val patternGenerator = PatternGenerator(colorIndex.sample, originalImage.width.toInt(), originalImage.height.toInt(),
                    periodicInput, symmetrySlider.value.toInt(), N, colorIndex.colorCount)

            val calculateIndices = patternGenerator.calculateIndices()
            calculateIndices.forEachIndexed { i, ints ->
                val square = ints.data.toTwoDimensions(N)
                val colors = square.map { outer -> outer.map { inner -> colorIndex.indexOf(inner).color }.toTypedArray() }.toTypedArray()
                patterns.add(Pattern(colors))
            }
        }
    }

    private fun createPatternFromPoint(x: Int, y: Int): Pattern {
        val pattern = Array(N, { Array(N) { Color.BLACK } })

        originalImage?.let { image ->
            if (x < image.width - N + 1 && y < image.height - N + 1) {
                for (xMod in 0 until N) for (yMod in 0 until N) {
                    val upperX = x + xMod
                    val upperY = y + yMod
                    pattern[yMod][xMod] = image.pixelReader.getColor(upperX, upperY)
                }
            }
        }
        return Pattern(pattern)
    }

    fun openFile() {
        val fileChooser = FileChooser()
        fileChooser.title = "Open image"
        fileChooser.extensionFilters.addAll(
                FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"))

        stage.widthProperty().addListener { observable, oldValue, newValue ->
            val stepAmount = (image.fitWidth / image.image.width.toInt()).toInt()
            val writeable = image.image as WritableImage
            resetImage()
            for (x in 0 until image.fitWidth.toInt()) {
                for (y in 0 until image.fitHeight.toInt()) {
                    if (x % stepAmount == 0 || y % stepAmount == 0) {
                        writeable.pixelWriter.setColor(x, y, Color.BLACK)
                    }
                }
            }
        }

        fileChooser.showOpenDialog(stage)?.let { loadFile(it) }
    }

    fun resetImage() {
        originalZoomedImage?.let { originalImage ->
            val w = originalImage.width.toInt()
            val h = originalImage.height.toInt()
            (image.image as WritableImage).pixelWriter.setPixels(0, 0, w, h, originalImage.pixelReader, 0, 0)
        }
    }

    fun drawBox(x: Int, y: Int, n: Int) {
        resetImage()
        val xMax = Math.min((x + n) * currentZoom, image.image.width.toInt())
        val yMax = Math.min((y + n) * currentZoom, image.image.height.toInt())
        for (x2 in x * currentZoom until xMax) {
            for (y2 in y * currentZoom until yMax) {
                val previousColor = image.image.pixelReader.getColor(x2, y2)
                (image.image as WritableImage).pixelWriter.setColor(x2, y2, previousColor.interpolate(Color.YELLOW, 0.75))
            }
        }
    }

    fun imageHoverMove(evt: MouseEvent) {
        val originalImage = this.originalImage ?: return
        val x = (evt.x / (image.boundsInParent.width / originalImage.width)).toInt()
        val y = (evt.y / (image.boundsInParent.height / originalImage.height)).toInt()
        val point = Pair(x, y)
        if (point != lastPoint) {
            drawBox(x, y, N)
            lastPoint = point
            val current = createPatternFromPoint(x, y)
            updateIndexFinder(x, y, current)
        }
    }

    fun periodicToggled() {
        calculatePatterns()
    }

    fun init() {
        addChangeListenerForIntSlider(sizeSlider) { newValue ->
            N = newValue
            calculatePatterns()
        }
        addChangeListenerForIntSlider(symmetrySlider) { newValue ->
            calculatePatterns()
        }
        symmetrySlider.labelFormatter = object : StringConverter<Double>() {
            override fun toString(obj: Double): String {
                return when (obj.toInt()) {
                    1 -> "Original only"
                    2 -> "Reflected"
                    3 -> "Rotated 90°"
                    4 -> "Rotated and mirrored"
                    5 -> "Rotated 180°"
                    6 -> "Rotated 180° and mirrored"
                    7 -> "Rotated 270°"
                    8 -> "Rotated 270° and mirrored"
                    else -> throw IllegalStateException()
                }
            }

            override fun fromString(string: String): Double {
                TODO("fromString")
            }
        }
        calculatePatterns()
        val recentFiles = Preferences.getRecentFiles()
        if (recentFiles.isNotEmpty()) {
            val menuItems = ArrayList<MenuItem>()
            menuItems.add(SeparatorMenuItem())
            recentFiles.forEachIndexed { i, path ->
                menuItems.add(MenuItem("${i + 1}. $path").apply {
                    onAction = EventHandler {
                        loadFile(path.toFile())
                    }
                })
            }
            menuItems.add(SeparatorMenuItem())
            fileMenu.items.addAll(fileMenu.items.size - 1, menuItems)
        }
        zoomSpinner.valueFactory = SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10, 1)
        currentZoom = zoomSpinner.value
        zoomSpinner.valueProperty().addListener { observableValue, old, new ->
            currentZoom = new
            zoomImage(new)
        }
        groundValue.onKeyTyped = EventHandler { groundCheckBox.isSelected = true }
    }

    private fun addChangeListenerForIntSlider(slider: Slider, cb: (value: Int) -> Unit) {
        var currentValue = slider.value.toInt()
        slider.valueChangingProperty().addListener { observableValue, wasChanging, isChanging ->
            val value = slider.value.toInt()
            if (!isChanging && value != currentValue) {
                currentValue = value
                cb(currentValue.toInt())
            }
        }
        slider.valueProperty().addListener { observableValue, old, new ->
            val value = slider.value.toInt()
            if (!slider.isValueChanging && value != currentValue) {
                currentValue = value
                cb(currentValue.toInt())
            }
        }
    }

    fun loadFile(selectedFile: File) {
        Preferences.rememberRecentFile(selectedFile.toPath())
        val readable = Image("file:" + selectedFile.toString())
        originalImage = readable

        zoomImage(zoomSpinner.value)
        calculatePatterns()
    }

    @FXML lateinit var renderWidthField: TextField
    @FXML lateinit var renderHeightField: TextField

    fun render() {
        originalImage?.let { originalImage ->
            val loader = FXMLLoader(javaClass.classLoader.getResource("render.fxml"))
            val root = loader.load<Parent>()
            val targetWidth = renderWidthField.text.toInt()
            val targetHeight = renderHeightField.text.toInt()
            val newStage = Stage().apply {
                scene = Scene(root, 800.0, 600.0)
            }
            val ground = if (groundCheckBox.isSelected)
                groundValue.text.toInt()
            else
                0
            loader.getController<RenderController>().apply {
                val overlappingModelParams = OverlappingModelParameters(originalImage.toBufferedImage())
                val overlappingModelInstanceParams = object : OverlappingModelInstanceParameters {
                    override val name = "whatever"
                    override val N = this@Controller.N
                    override val width = targetWidth
                    override val height = targetHeight
                    override val symmetry = symmetrySlider.value.toInt()
                    override val periodicInput = true
                    override val periodicOutput = this@Controller.periodicCheckBox.isSelected
                    override val ground = ground
                    override val observeCallback = {}
                    override val propagateCallback = {}
                }
                init(overlappingModelParams, overlappingModelInstanceParams, newStage)
            }
            newStage.show()
        }
    }

    fun zoomImage(factor: Int) {
        originalImage?.let { originalImage ->
            val zoomedImage = originalImage.zoom(factor)
            this.zoomedImage = zoomedImage
            originalZoomedImage = WritableImage(zoomedImage.pixelReader, zoomedImage.width.toInt(), zoomedImage.height.toInt())
            image.apply {
                this.image = zoomedImage
                fitWidth = this.image.width
                fitHeight = this.image.height
            }
        }
    }

    fun close() {
        Platform.exit()
    }
}

private fun Image.toBufferedImage() = BufferedImage(width.toInt(), height.toInt(), BufferedImage.TYPE_INT_RGB).apply {
    SwingFXUtils.fromFXImage(this@toBufferedImage, this)
}
