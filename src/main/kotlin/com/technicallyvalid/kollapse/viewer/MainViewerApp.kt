/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class MainViewerApp : Application() {

    @Throws(Exception::class)
    override fun start(primaryStage: Stage) {
        val loader = FXMLLoader(javaClass.classLoader.getResource("main.fxml"))
        val root = loader.load<Parent>()
        primaryStage.title = "Kollapse Viewer"
        primaryStage.scene = Scene(root, 800.0, 600.0)
        primaryStage.show()
        loader.getController<Controller>().apply {
            stage = primaryStage
            init()
        }
    }

    fun myLaunch(args: Array<String>) {
        Application.launch(*args)
    }

    companion object {
        @JvmStatic fun main(args: Array<String>) {
            MainViewerApp().myLaunch(args)
        }
    }
}
