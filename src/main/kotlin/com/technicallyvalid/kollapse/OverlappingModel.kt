/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.awt.image.BufferedImage
import java.util.*

class OverlappingModel(params: OverlappingModelParameters, instanceParams: OverlappingModelInstanceParameters) : Model() {
    override val FMX = instanceParams.width
    override val FMY = instanceParams.height
    override val periodic = instanceParams.periodicOutput
    private val periodicInput = instanceParams.periodicInput
    private val symmetry = instanceParams.symmetry

    private val propagator: Array<Array<Array<IntArray>>>
    private val N: Int
    private val patterns: Array<IntArray>
    private val ground: Int

    private val onObserve = instanceParams.observeCallback
    private val onPropagate = instanceParams.propagateCallback

    private val colorIndexer: ColorIndexer<BufferedImage, AwtColorWrapper>

    init {
        N = instanceParams.N

        val bitmap = params.image
        val smx = bitmap.width
        val smy = bitmap.height

        colorIndexer = ColorIndexer(AwtBufferedImageWrapper(bitmap))
        val sample = colorIndexer.sample
        val C = colorIndexer.colorCount

        val W = Math.pow(C.toDouble(), (N * N).toDouble()).toInt()

        val weights = PatternGenerator(sample, smx, smy, periodicInput, symmetry, N, C).calculateWeights()

        T = weights.size
        ground = (instanceParams.ground + T) % T

        patterns = Array(T, { IntArray(0) })
        stationary = DoubleArray(T)
        propagator = Array(T, { Array(2 * N - 1, { Array(2 * N - 1) { IntArray(0) } }) })
        weights.entries.forEachIndexed { i, entry ->
            val (w, value) = entry
            patterns[i] = patternFromIndex(w, C, W)
            stationary[i] = value.toDouble()
        }

        wave = Array(FMX) { Array(FMY) { BooleanArray(T) { true } } }
        changes = Array(FMX) { BooleanArray(FMY) { false } }

        for (t in 0 until T) {
            propagator[t] = Array(2 * N - 1) { Array(0) { IntArray(0) } }
            for (x in 0 until (2 * N - 1)) {
                propagator[t][x] = Array(2 * N - 1) { IntArray(0) }
                for (y in 0 until (2 * N - 1)) {
                    val list = ArrayList<Int>()
                    for (t2 in 0 until T) {
                        if (agrees(patterns[t], patterns[t2], x - N + 1, y - N + 1)) list.add(t2)
                    }
                    propagator[t][x][y] = IntArray(list.size)
                    for (c in 0 until list.size) propagator[t][x][y][c] = list[c]
                }
            }
        }
    }

    private fun patternFromIndex(ind: Int, C: Int, W: Int): IntArray {
        var residue = ind
        var power = W.toInt()
        val result = IntArray(N * N)

        for (i in 0 until result.size) {
            // TODO I'm pretty sure this whole thing is...just modulo?
            power /= C
            var count = 0
            while (residue >= power) {
                residue -= power
                count++
            }
            result[i] = count
        }
        return result
    }

    private fun agrees(p1: IntArray, p2: IntArray, dx: Int, dy: Int): Boolean {
        val xmin = if (dx < 0) 0 else dx
        val xmax = if (dx < 0) dx + N else N
        val ymin = if (dy < 0) 0 else dy
        val ymax = if (dy < 0) dy + N else N

        for (y in ymin until ymax) for (x in xmin until xmax) {
            if (p1[x + N * y] != p2[x - dx + N * (y - dy)]) return false
        }
        return true
    }

    override fun observe(): Boolean? {
        val original = super.observe()
        onObserve()
        return original
    }

    override fun propagate(): Boolean {
        var change = false

        for (x1 in 0 until FMX) {
            for (y1 in 0 until FMY) {
                if (changes[x1][y1]) {
                    changes[x1][y1] = false

                    for (dx in (-N + 1) until N) for (dy in (-N + 1) until N) {
                        val x2 = x1 + dx
                        val y2 = y1 + dy

                        val sx = if (x2 < 0) // TODO possible optimization -- same as x2 % FMX?
                            x2 + FMX
                        else if (x2 >= FMX)
                            x2 - FMX
                        else
                            x2

                        val sy = if (y2 < 0)
                            y2 + FMY
                        else if (y2 >= FMY)
                            y2 - FMY
                        else
                            y2

                        if (!periodic && (sx + N > FMX || sy + N > FMY)) continue
                        val waveAllowed = wave[sx][sy]

                        for (t2 in 0 until T) {
                            if (!waveAllowed[t2]) {
                                continue
                            }
                            val prop = propagator[t2][N - 1 - dx][N - 1 - dy]
                            val b = (0 until prop.size).any { i1 -> wave[x1][y1][prop[i1]] }
                            if (!b) {
                                changes[sx][sy] = true
                                change = true
                                waveAllowed[t2] = false
                            }
                        }
                    }
                }
            }
        }
        onPropagate()
        return change
    }

    override fun onBoundary(x: Int, y: Int) = !periodic && (x + N > FMX || y + N > FMY)

    override fun <I, T : WritableImage<I>> renderTo(renderer: Renderer<T>): I {
        val image = renderer.createImage(FMX, FMY)
        val contributors = ArrayList<Int>()
        val data = IntArray(FMX * FMY * 3)
        var idx = 0
        for (y in 0 until FMY) {
            for (x in 0 until FMX) {
                contributors.clear()
                for (dy in 0 until N) for (dx in 0 until N) {
                    val sx = (x - dx).let { if (it < 0) it + FMX else it }
                    val sy = (y - dy).let { if (it < 0) it + FMY else it }

                    if (onBoundary(sx, sy)) continue
                    val newContributors = (0 until T).filter { t -> wave[sx][sy][t] }.map { t -> patterns[t][dx + dy * N] }
//                    newContributors.forEach { contributors.add(it) }
                    if (newContributors.isNotEmpty()) contributors.addAll(newContributors)
                }
                var r = 0.0
                var g = 0.0
                var b = 0.0

                val lambda = 1.0 / contributors.size
                for (c in contributors) {
                    val color = colorIndexer.indexOf(c)
                    r += color.red * lambda
                    g += color.green * lambda
                    b += color.blue * lambda
                }
//                image.setPixel(x, y, r.toInt(), g.toInt(), b.toInt())
//                data[idx++] = Color(r.toInt(), g.toInt(), b.toInt()).rgb
                data[idx++] = r.toInt()
                data[idx++] = g.toInt()
                data[idx++] = b.toInt()
            }
        }
        (image.image as BufferedImage).raster.setPixels(0, 0, FMX, FMY, data)
        return image.image
    }

    override fun clear() {
        super.clear()

        if (ground != 0) {
            for (x in 0 until FMX) {
                for (t in 0 until T) {
                    if (t != ground) {
                        wave[x][FMY - 1][t] = false
                    }
                }
                changes[x][FMY - 1] = true

                for (y in 0 until FMY - 1) {
                    wave[x][y][ground] = false
                    changes[x][y] = true
                }

            }
            while (propagate()) {
                // repeat until propagated
            }
        }
    }
}


