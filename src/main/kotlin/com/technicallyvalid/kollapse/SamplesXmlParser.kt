/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

package com.technicallyvalid.kollapse

import org.w3c.dom.Element
import org.w3c.dom.Node
import java.nio.file.Path
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathFactory

class SamplesXmlParser(path: Path) {
    private val xpath: XPath = XPathFactory.newInstance().newXPath()
    private val doc: Element

    init {
        val dbf = DocumentBuilderFactory.newInstance()
        val db = dbf.newDocumentBuilder()
        doc = db.parse(path.toFile()).documentElement
    }

    fun parse(): List<RunnerParameters> {
        return doc.nodeListByXPath("//simpletiled", xpath).asList().map { parseSimpleTiled(it) }.plus(
                doc.nodeListByXPath("//overlapping", xpath).asList().map { parseOverlapping(it) }
        )
    }

    private fun parseOverlapping(node: Node): OverlappingModelSampleParameters {
        val name = requireNotNull(node["name"]) { "no name given!" }
        return OverlappingModelSampleParameters(name).apply {
            node["N"]?.let { N = it.toInt()}
            node["width"]?.let { width = it.toInt() }
            node["height"]?.let { height = it.toInt() }
            node["periodic"]?.let { periodicOutput = it.toBoolean() }
            node["periodicInput"]?.let { periodicInput = it.toBoolean() }
            node["symmetry"]?.let { symmetry = it.toInt() }
            node["ground"]?.let { ground = it.toInt() }

            node["limit"]?.let { limit = it.toInt() }
            node["screenshots"]?.let { screenshots = it.toInt() }
        }
    }

    private fun parseSimpleTiled(node: Node): SimpleTiledModelSampleParameters {
        val name = requireNotNull(node["name"]) { "no name given!" }
        return SimpleTiledModelSampleParameters(name).apply {
            subset = node["subset"]

            node["width"]?.let { width = it.toInt() }
            node["height"]?.let { height = it.toInt() }
            node["periodic"]?.let { periodic = it.toBoolean() }
            node["limit"]?.let { limit = it.toInt() }
            node["screenshots"]?.let { screenshots = it.toInt() }
        }
    }
}