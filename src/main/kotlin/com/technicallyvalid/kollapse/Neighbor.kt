/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

data class Neighbor(val left: String, val leftQ: Int = 0, val right: String, val rightQ: Int = 0) {
    constructor(left: String, right: String) : this(left, 0, right, 0)
    constructor(left: String, right: String, rightQ: Int) : this(left, 0, right, rightQ)
    constructor(left: String, leftQ: Int, right: String) : this(left, leftQ, right, 0)

    init {
        require(!left.contains(' ')) { "left should not contain spaces, was $left" }
        require(!right.contains(' ')) { "right should not contain spaces, was $right" }
    }
}