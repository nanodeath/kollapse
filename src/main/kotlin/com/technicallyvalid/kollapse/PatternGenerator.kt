/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.util.*

class PatternGenerator(val sample: Array<IntArray>, val smx: Int, val smy: Int, val periodicInput: Boolean, val symmetry: Int, val regionSize: Int, val colorCount: Int) {
    data class BetterIntArray(val data: IntArray) {
        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other !is BetterIntArray) return false

            return Arrays.equals(data, other.data)
        }

        override fun hashCode(): Int {
            return Arrays.hashCode(data)
        }
    }

    fun calculateIndices(): LinkedHashSet<BetterIntArray> {
        val indices = LinkedHashSet<BetterIntArray>()
        calculate() { permutation ->
            indices.add(BetterIntArray(permutation))
        }
        return indices
    }

    fun calculateWeights(): LinkedHashMap<Int, Int> {
        val weights = LinkedHashMap<Int, Int>()
        calculate() { permutation ->
            val ind = permutation.index(colorCount)
            weights[ind] = (weights[ind] ?: 0) + 1
        }
        return weights
    }

    private fun calculate(cb: (permutation: IntArray) -> Unit) {
        for (y in 0 until (if (periodicInput) smy else (smy - regionSize + 1))) {
            for (x in 0 until (if (periodicInput) smx else (smx - regionSize + 1))) {
                val ps = Array(8, { IntArray(0) })
                ps[0] = patternFromSample(sample, smx, smy, x, y)
                ps[1] = ps[0].reflect()
                ps[2] = ps[0].rotate()
                ps[3] = ps[2].reflect()
                ps[4] = ps[2].rotate()
                ps[5] = ps[4].reflect()
                ps[6] = ps[4].rotate()
                ps[7] = ps[6].reflect()

                for (k in 0 until symmetry) {
                    cb(ps[k])
                }
            }
        }
    }

    private fun pattern(f: (Int, Int) -> Int) = IntArray(regionSize * regionSize) { i -> f(i % regionSize, i / regionSize) }

    private fun patternFromSample(sample: Array<IntArray>, smx: Int, smy: Int, x: Int, y: Int): IntArray {
        return pattern { dx, dy -> sample[(x + dx) % smx][(y + dy) % smy] }
    }

    private fun IntArray.rotate(): IntArray = pattern { x, y -> this[regionSize - 1 - y + x * regionSize] }

    private fun IntArray.reflect(): IntArray = pattern { x, y -> this[regionSize - 1 - x + y * regionSize] }

    private fun IntArray.index(C: Int): Int {
        var result = 0
        var power = 1
        for (i in 0 until size) {
            result += this[lastIndex - i] * power
            power *= C
        }
        return result
    }
}