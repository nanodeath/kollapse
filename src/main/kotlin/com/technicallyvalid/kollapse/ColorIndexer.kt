/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

class ColorIndexer<I, out T : Pixel>(bitmap: ReadableImage<I, T>) {
    private val colorIndices = IndexedMap<T>()
    val colorCount: Int
    val sample: Array<IntArray>

    init {
        val smx = bitmap.width
        val smy = bitmap.height
        sample = Array(smx, { IntArray(smy) })

        for (y in 0 until smy) for (x in 0 until smx) {
            val color = bitmap.getPixel(x, y)
            val i = colorIndices.getOrMakeIndexForKey(color)
            sample[x][y] = i
        }

        colorCount = colorIndices.size
    }

    fun indexOf(c: Int): T = colorIndices[c]
}