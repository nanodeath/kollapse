/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.awt.image.BufferedImage
import java.nio.file.Path
import java.util.*

interface ModelParameters {}

class SimpleTiledModelParameters: ModelParameters {
    var tileSize = 16
    var unique = false
    var tiles: List<Tile> = arrayListOf()
    var neighbors: List<Neighbor> = arrayListOf()
    var subsets: Map<String, Subset> = emptyMap()

    var tileToFile: (Tile) -> Path = { t -> throw NotImplementedError("Document#tileToFile not provided") }
    var tileCardToFile: (Tile, Int) -> Path = { t, i -> throw NotImplementedError("Document#tileToCardFile not provided") }

    fun validate() {
        require(tiles.isNotEmpty()) { "tiles must not be empty" }
        require(neighbors.isNotEmpty()) { "neighbors must not be empty" }
    }
}

class OverlappingModelParameters(val image: BufferedImage): ModelParameters {
}

interface SimpleTiledModelInstanceParameters {
    val name: String
    val subset: String?
    val width: Int
    val height: Int
    val periodic: Boolean
    val black: Boolean
}

interface OverlappingModelInstanceParameters {
    val name: String
    val N: Int
    val width: Int
    val height: Int
    val symmetry: Int
    val periodicInput: Boolean
    val periodicOutput: Boolean
    val ground: Int
    val observeCallback: () -> Unit
    val propagateCallback: () -> Unit
}

interface RunnerParameters {
    val maxRetries: Int
    val limit: Int
    val screenshots: Int
    val random: Random
}

class SimpleTiledModelSampleParameters(override val name: String) : SimpleTiledModelInstanceParameters, RunnerParameters {
    override var subset: String? = null
    override var width = 16
    override var height = 16
    override var periodic = false
    override var black = false

    override var limit = Int.MAX_VALUE
    override var maxRetries = 10
    override var screenshots = 1
    override var random = Random()
}

class OverlappingModelSampleParameters(override val name: String) : OverlappingModelInstanceParameters, RunnerParameters {
    override var N = 2
    override var symmetry = 8
    override var periodicInput = true
    override var periodicOutput = false
    override var ground = 0
    override var width = 48
    override var height = 48

    override var limit = Int.MAX_VALUE
    override var maxRetries = 10
    override var screenshots = 1
    override var random = Random()

    override val observeCallback = {}
    override val propagateCallback = {}
}

data class Subset(val tiles: List<Tile>)