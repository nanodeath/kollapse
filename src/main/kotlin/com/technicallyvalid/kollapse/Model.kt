/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.util.*

interface ReadableImage<out I, out T : Pixel> {
    val image: I
    val width: Int
    val height: Int
    fun getPixel(x: Int, y: Int): T
}

interface WritableImage<out I> {
    val image: I
    fun setPixel(x: Int, y: Int, red: Int, green: Int, blue: Int)
}

/**
 * Implementors must override hashcode/equals! This object is used as keys in hashes.
 */
interface Pixel {
    val red: Int
    val green: Int
    val blue: Int
}

abstract class Model {
    // TODO make these immutable too
    protected lateinit var wave: Array<Array<BooleanArray>>
    protected lateinit var changes: Array<BooleanArray>
    protected lateinit var stationary: DoubleArray

    protected lateinit var random: Random
    protected abstract val FMX: Int
    protected abstract val FMY: Int
    protected var T: Int = 0
    protected abstract val periodic: Boolean

    internal lateinit var logProb: DoubleArray
    internal var logT: Double = 0.0

    abstract fun propagate(): Boolean
    abstract fun onBoundary(x: Int, y: Int): Boolean
    abstract fun <I, T : WritableImage<I>> renderTo(renderer: Renderer<T>): I

    open fun observe(): Boolean? {
        var min = 1e3
        var sum: Double
        var mainSum: Double
        var logSum: Double
        var noise: Double
        var entropy: Double

        var argminx = -1
        var argminy = -1
        var amount: Int

        for (x in 0 until FMX) {
            for (y in 0 until FMY) {
                if (onBoundary(x, y)) continue
                amount = 0
                sum = 0.0

                for (t in 0 until T) {
                    if (wave[x][y][t]) {
                        amount += 1
                        sum += stationary[t]
                    }
                }

                if (sum == 0.0) {
                    return false
                }

                noise = 1e-6 * random.nextDouble()

                entropy = when (amount) {
                    1 -> 0.0
                    T -> logT
                    else -> {
                        logSum = Math.log(sum)
                        mainSum = (0 until T).filter { t -> wave[x][y][t] }.map { t -> stationary[t] * logProb[t] }.sum()
                        logSum - mainSum / sum
                    }
                }

                if (entropy > 0.0 && entropy + noise < min) {
                    min = entropy + noise
                    argminx = x
                    argminy = y
                }
            }
        }

        if (argminx == -1 && argminy == -1) return true

        val distribution = (0 until T).map { t -> if (wave[argminx][argminy][t]) stationary[t] else 0.0 }.toDoubleArray()
        val r = distribution.randomDistribution(random.nextDouble())
        for (t in 0 until T) {
            wave[argminx][argminy][t] = t == r
        }
        changes[argminx][argminy] = true

        return null
    }

    /**
     * Runs the simulation. Randomness is seeded by the given [seed], and the simulation is run [limit] times.
     *
     * @param seed seed for the random number generator. Using the same seed will yield same output.
     * @param limit maximum number of times to call [observe]. Defaults to infinity.
     *
     * @return true if terminated successfully, or false if a contradiction occurred.
     */
    @JvmOverloads
    fun run(seed: Long, limit: Int = Int.MAX_VALUE): Boolean {
        require(limit > 0) { "limit must be > 0, was $limit" }
        logT = Math.log(T.toDouble())
        logProb = DoubleArray(T) { t -> Math.log(stationary[t]) }

        clear()

        random = Random(seed)

        repeat(limit) {
            val result = observe()
            if (result != null) return result
            while (propagate()) {
                // nothing, just run propagate() until it returns false
            }
        }
        return true
    }

    open protected fun clear() {
        // TODO maybe cache lookups?
        for (x in 0 until FMX) for (y in 0 until FMY) {
            for (t in 0 until T) wave[x][y][t] = true
            changes[x][y] = false
        }
    }

    private fun DoubleArray.randomDistribution(r: Double): Int {
        val sum = this.sum()
        if (sum == 0.0) {
            return (r * size).toInt()
        }
        var x = 0.0
        val rNormalized = r * sum
        forEachIndexed { i, d ->
            x += d
            if (rNormalized <= x) return i
        }
        return 0
    }
}


