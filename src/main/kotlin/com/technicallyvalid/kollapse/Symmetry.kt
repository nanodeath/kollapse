/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

enum class Symmetry {
    L {
        override val cardinality = 4
        override fun a(i: Int) = (i + 1) % 4
        override fun b(i: Int) = if (i.isEven()) i + 1 else i - 1
    },
    T {
        override val cardinality = 4
        override fun a(i: Int) = (i + 1) % 4
        override fun b(i: Int) = if (i.isEven()) i else 4 - i
    },
    I {
        override val cardinality = 2
        override fun a(i: Int) = 1 - i
        override fun b(i: Int) = i
    },
    BACKSLASH { // instead of "\", which isn't a valid Java identifier
        override val cardinality = 2
        override fun a(i: Int) = 1 - i
        override fun b(i: Int) = 1 - i
    },
    X {
        override val cardinality = 1
        override fun a(i: Int) = i
        override fun b(i: Int) = i
    };

    abstract val cardinality: Int
    abstract fun a(i: Int): Int
    abstract fun b(i: Int): Int

    protected fun Int.isEven() = this % 2 == 0

    companion object {
        fun parse(str: String): Symmetry {
            return when (str) {
                "\\" -> BACKSLASH
                else -> valueOf(str)
            }
        }
    }
}