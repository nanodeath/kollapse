/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

package com.technicallyvalid.kollapse

import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.nio.file.Path
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathFactory

class XmlLoader {
    private val db: DocumentBuilder
    private val xpath: XPath = XPathFactory.newInstance().newXPath()

    init {
        val dbf = DocumentBuilderFactory.newInstance()
        db = dbf.newDocumentBuilder()
    }

    fun parseFile(xmlFile: Path): SimpleTiledModelParameters {
        val doc = db.parse(xmlFile.toFile()).documentElement
        val size = doc["size"]!!.toInt()
        val isUnique = doc["unique"]?.toBoolean() ?: false

        return SimpleTiledModelParameters().apply {
            unique = isUnique
            tileSize = size
            tiles = parseAllTiles(doc)
            neighbors = parseAllNeighbors(doc)
            subsets = parseAllSubsets(doc)
        }
    }

    private fun parseAllTiles(doc: Element): List<Tile> {
        val tileNodes = doc.nodeListByXPath("/set/tiles/tile")
        return tileNodes.asList().map { parseTile(it) }
    }

    private fun parseAllNeighbors(doc: Element): List<Neighbor> {
        val neighborNodes = doc.nodeListByXPath("/set/neighbors/neighbor")
        return neighborNodes.asList().map { item ->
            val left = item["left"]?.split(' ') ?: throw IllegalArgumentException("neighbor missing left attribute")
            val right = item["right"]?.split(' ') ?: throw IllegalArgumentException("neighbor missing right attribute")
            // This is sorta dumb/verbose
            if (left.size > 1 && right.size > 1) {
                Neighbor(left[0], left[1].toInt(), right[0], right[1].toInt())
            } else if (left.size > 1) {
                Neighbor(left[0], left[1].toInt(), right[0])
            } else if (right.size > 1) {
                Neighbor(left[0], right[1].toInt(), right[0])
            } else {
                Neighbor(left[0], right[0])
            }
        }
    }

    private fun parseAllSubsets(doc: Element): Map<String, Subset> {
        val subsetNodes = doc.nodeListByXPath("/set/subsets/subset")
        return subsetNodes.asList().associateBy(
                { item -> item["name"]!! },
                { item -> Subset(item.elementChildNodes.map { parseTile(it) }) })
    }

    /**
     * Parse an [item] (a &lt;tile&gt;) into a [Tile].
     */
    private fun parseTile(item: Node): Tile {
        val name = item["name"]!!
        val symmetry = item["symmetry"]
        val weight = item["weight"]?.toDouble()
        return Tile(name, symmetry, weight)
    }

    internal fun Element.nodeListByXPath(xpathQuery: String): NodeList = nodeListByXPath(xpathQuery, xpath)
}