/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.awt.Color
import java.nio.file.Files
import java.util.*
import javax.imageio.ImageIO

class SimpleTiledModel(params: SimpleTiledModelParameters, instanceParams: SimpleTiledModelInstanceParameters) : Model() {
    private val unique: Boolean
    private var black: Boolean = instanceParams.black // TODO
    private val subset: MutableSet<String> = HashSet()
    private val propagator: Array<Array<BooleanArray>>
    private val tileSize: Int
    private val tiles: MutableList<Array<Color>> = ArrayList()
    private val subsetName = instanceParams.subset
    private val useSubset = subsetName != null
    override val FMX = instanceParams.width
    override val FMY = instanceParams.height
    override val periodic = instanceParams.periodic

    init {
        unique = params.unique
        tileSize = params.tileSize
        val action = ArrayList<IntArray>()
        val firstOccurrence = HashMap<String, Int>()
        val tempStationary = ArrayList<Double>()

        if (subsetName != null) {
            val subsetSection = requireNotNull(params.subsets[subsetName]) { "Subset not found: $subsetName" }
            subset.addAll(subsetSection.tiles.map(Tile::name).toSet())
        }

        val subset = this.subset
        for (tile in params.tiles) {
            val tileName = tile.name
            if (useSubset && !subset.contains(tileName)) continue

            val rawSymmetry = tile.symmetry ?: "X"
            val symmetry = Symmetry.parse(rawSymmetry)

            val currentT = action.size
            firstOccurrence.put(tileName, currentT)
            action.addAll(generateActionMapForTile(symmetry, currentT))
            if (unique) {
                processUnique(params, symmetry, tile)
            } else {
                processNonUnique(params, symmetry, tile, currentT)
            }
            for (i in 0 until symmetry.cardinality) {
                tempStationary.add(tile.weight ?: 1.0)
            }
        }

        T = action.size
        stationary = tempStationary.toDoubleArray()
        propagator = Array(4) { Array(T) { BooleanArray(T) } }
        wave = Array(FMX) { Array(FMY) { BooleanArray(T) } }
        changes = Array(FMX) { BooleanArray(FMY) }

        for ((left, leftQ, right, rightQ) in params.neighbors) {
            if (useSubset && (!subset.contains(left) || !subset.contains(right))) continue

            val L = action[firstOccurrence[left]!!][leftQ]
            val D = action[L][1]
            val R = action[firstOccurrence[right]!!][rightQ]
            val U = action[R][1]

            propagator[0].apply {
                this[L][R] = true
                this[action[L][6]][action[R][6]] = true
                this[action[R][4]][action[L][4]] = true
                this[action[R][2]][action[L][2]] = true
            }
            propagator[1].apply {
                this[D][U] = true
                this[action[U][6]][action[D][6]] = true
                this[action[D][4]][action[U][4]] = true
                this[action[U][2]][action[D][2]] = true
            }
        }

        for (t1 in 0 until T) {
            for (t2 in 0 until T) {
                propagator[2][t1][t2] = propagator[0][t2][t1]
                propagator[3][t1][t2] = propagator[1][t2][t1]
            }
        }
    }

    private fun generateActionMapForTile(sym: Symmetry, currentT: Int): Array<IntArray> {
        return Array(sym.cardinality) { t ->
            arrayOf(
                    currentT + t,
                    currentT + sym.a(t),
                    currentT + sym.a(sym.a(t)),
                    currentT + sym.a(sym.a(sym.a(t))),
                    currentT + sym.b(t),
                    currentT + sym.b(sym.a(t)),
                    currentT + sym.b(sym.a(sym.a(t))),
                    currentT + sym.b(sym.a(sym.a(sym.a(t))))
            ).toIntArray()
        }
    }

    private fun processNonUnique(mainData: SimpleTiledModelParameters, sym: Symmetry, tile: Tile, currentT: Int) {
        val path = mainData.tileToFile(tile)
        require(Files.exists(path) && Files.isRegularFile(path)) { "Could not find file for ${tile.name}, looked for $path" }
        val image = ImageIO.read(path.toFile())
        tiles.add(tile { x, y -> Color(image.getRGB(x, y)) })
        for (t in 1 until sym.cardinality) {
            tiles.add(rotate(tiles[currentT + t - 1]))
        }
    }

    private fun processUnique(mainData: SimpleTiledModelParameters, sym: Symmetry, tile: Tile) {
        for (t in 0 until sym.cardinality) {
            val path = mainData.tileCardToFile(tile, t)
            require(Files.exists(path) && Files.isRegularFile(path)) { "Could not find file for ${tile.name}-$t, looked for $path" }
            val image = ImageIO.read(path.toFile())
            tiles.add(tile { x, y -> Color(image.getRGB(x, y)) })
        }
    }

    override fun propagate(): Boolean {
        var change = false

        for (x2 in 0 until FMX) {
            for (y2 in 0 until FMY) {
                loop@ for (d in 0 until 4) {
                    var x1 = x2
                    var y1 = y2
                    when (d) {
                        0 -> {
                            x1 = if (x2 == 0) {
                                if (!periodic) continue@loop
                                FMX - 1
                            } else {
                                x2 - 1
                            }
                        }
                        1 -> {
                            y1 = if (y2 == FMY - 1) {
                                if (!periodic) continue@loop
                                0
                            } else {
                                y2 + 1
                            }
                        }
                        2 -> {
                            x1 = if (x2 == FMX - 1) {
                                if (!periodic) continue@loop
                                0
                            } else {
                                x2 + 1
                            }
                        }
                        3 -> {
                            y1 = if (y2 == 0) {
                                if (!periodic) continue@loop
                                FMY - 1
                            } else {
                                y2 - 1
                            }
                        }
                    }
                    if (!changes[x1][y1]) continue

                    for (t2 in 0 until T) {
                        if (wave[x2][y2][t2]) {
                            val waveAndPropagate = (0 until T).any { t1 -> wave[x1][y1][t1] && propagator[d][t1][t2] }
                            if (!waveAndPropagate) {
                                wave[x2][y2][t2] = false
                                changes[x2][y2] = true
                                change = true
                            }
                        }
                    }
                }
            }
        }
        return change
    }

    override fun onBoundary(x: Int, y: Int) = false

    override fun <I, T : WritableImage<I>> renderTo(renderer: Renderer<T>): I {
        val image = renderer.createImage(FMX * tileSize, FMY * tileSize)
        for (x in 0 until FMX) {
            for (y in 0 until FMY) {
                val a = wave[x][y]
                val amount = a.asSequence().filter { it }.count()
                val lambda = 1.0 / (0 until T).filter { t -> a[t] }.map { t -> stationary[t] }.sum()

                for (yt in 0 until tileSize) {
                    for (xt in 0 until tileSize) {
                        if (black && amount == T) {
                            val blackColor = Color.black
                            image.setPixel(x * tileSize + xt, y * tileSize + yt, blackColor.red, blackColor.green, blackColor.blue)
                        } else {
                            var (r, g, b) = Triple(0.0, 0.0, 0.0)
                            for (t in 0 until T) {
                                if (wave[x][y][t]) {
                                    val c = tiles[t][xt + yt * tileSize]
                                    // TODO factor out stationary[t] * lambda
                                    r += c.red * stationary[t] * lambda
                                    g += c.green * stationary[t] * lambda
                                    b += c.blue * stationary[t] * lambda
                                }
                            }
                            image.setPixel(x * tileSize + xt, y * tileSize + yt, r.toInt(), g.toInt(), b.toInt())
                        }
                    }
                }
            }
        }
        return image.image
    }

    private fun tile(cb: (x: Int, y: Int) -> Color): Array<Color> {
//        val result = Array(tilesize * tilesize) { Color.BLACK }
//        for (y in 0 until tilesize) for (x in 0 until tilesize) {
//            result[x + y * tilesize] = cb(x, y)
//        }
//        return result

        // Above and below are exactly equivalent, but below is more concise. Not sure if actually faster, though, due
        // to the multiple division-type operations. Probably overthinking it.
        return Array(tileSize * tileSize) { i -> cb(i % tileSize, i / tileSize) }
    }

    private fun rotate(data: Array<Color>): Array<Color> {
        return tile { x, y -> data[tileSize - 1 - y + x * tileSize] }
    }
}